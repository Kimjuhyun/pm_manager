package com.smartjack.pennymany.manager.utils;

import java.util.regex.Pattern;

/**
 * Created by rlawn on 2017-10-28.
 */

public class ValidUtils {
    public static boolean email(String email) {
        if (email==null) return false;
        boolean b = Pattern.matches(
                "[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+",
                email.trim());
        return b;
    }
}
