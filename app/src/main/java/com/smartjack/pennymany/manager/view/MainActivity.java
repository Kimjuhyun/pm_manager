package com.smartjack.pennymany.manager.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.smartjack.pennymany.manager.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
