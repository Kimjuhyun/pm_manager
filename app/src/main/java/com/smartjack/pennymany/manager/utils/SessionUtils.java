package com.smartjack.pennymany.manager.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;


/**
 * Created by 주현 on 2016-01-18.
 */
public class SessionUtils {

    public final static String TOKEN = "TOKEN";
    public final static String TEST_TOKEN = "AAAAB3NzaC1yc2EAAAADAQABAAABAQCzvMKnZEdWDYyzWNRN6D8xcIRbmg9gIpH7LxFQf5X3S/NTpPjlOPDAXOUkGIs3ZXvvG0LAsqDxUqa8dAsnMBqqmzvYLpTLXaDxmK";

    public final static String ND_CLIENT_ID = "kFPF70UoDT0gb7_kBFoD";
    public final static String ND_SECRET = "R2pvQ8M_cK";

    public static void putString(Context context, String key , String value){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(key,value);
        editor.commit();
    }
    public static void putBoolean(Context context, String key , boolean value){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(key,value);
        editor.commit();
    }
    public static void putLong(Context context, String key , long value){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(key,value);
        editor.commit();
    }
    public static void putInt(Context context, String key , int value){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(key,value);
        editor.commit();
    }
    public static void remove(Context context, String key){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.remove(key);
        editor.commit();
    }
    public static String getString(Context context, String key, String defValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(key,defValue);
    }
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getBoolean(key,defValue);
    }
    public static long getLong(Context context, String key, long defValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getLong(key,defValue);
    }
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(key,defValue);
    }
    public static boolean checkToken(Context context){
        return checkToken(context,true);
    }
    public static boolean checkToken(final Context context, boolean toast){
        if(getString(context,TOKEN,TEST_TOKEN).equals(TEST_TOKEN)){
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setMessage("로그인 후 이용하실 수 있습니다.\n로그인 페이지로 이동하시겠습니까?");
//            alert.setPositiveButton("예", new DialogInterface.OnClickListener(){
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Intent intent = new Intent(context, LoginActivity.class);
//                    context.startActivity(intent);
//                    dialog.dismiss();
//                }
//            });

            alert.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
            return false;
        }
        return true;
    }
}
